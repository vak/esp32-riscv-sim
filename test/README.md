# RISC-V Tests

The architectural RISC-V tests were imported from official
[riscv-arch-test](https://github.com/riscv/riscv-arch-test) repo.

Directory structure
```
.                         # top level folder contains the architectural test header files
├── rv32i                 # tests for RV32I instruction set
│   ├── references        # references signatures for RV32I instruction set
│   ├── src               # assembly sources for RV32I instruction set
│   └── trace             # trace logs for RV32I instruction set
├── c_extension           # tests for "C" extension
│   ├── references        # reference signatures for "C" extension
│   ├── src               # assembly sources for "C" extension
│   └── trace             # trace logs for "C" extension
├── m_extension           # tests for "M" extension
│   ├── references        # reference signatures for "M" extension
│   ├── src               # assembly sources for "M" extension
│   └── trace             # trace logs for "M" extension
└── privilege             # tests which require Privilege Spec
    ├── references        # reference signatures for Privilege tests
    ├── src               # assembly sources for Privilege tests
    └── trace             # trace logs for Privilege tests
```

Tests use Googletest framework. Here is a list of test modules:

 * base_tests.cpp - tests for RV32I instruction set
 * c_tests.cpp - tests for "C" extension
 * disasm_test.cpp - tests for disassembler
 * m_tests.cpp - tests for "M" extension
 * privilege_tests.cpp - tests which require Privilege Spec
 * smoke_test.cpp - check the SystemC library coexists happily with Googletest
