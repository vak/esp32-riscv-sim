#include <gtest/gtest.h>
#include <systemc.h>

#include "simulator.h"

TEST(c_extension, cadd_test)
{
    test_with_reference("c_extension/cadd-01.elf",
                        "c_extension/references/cadd-01.reference_output");
}

TEST(c_extension, caddi_test)
{
    test_with_reference("c_extension/caddi-01.elf",
                        "c_extension/references/caddi-01.reference_output");
}

TEST(c_extension, caddi16sp_test)
{
    test_with_reference("c_extension/caddi16sp-01.elf",
                        "c_extension/references/caddi16sp-01.reference_output");
}

TEST(c_extension, caddi4spn_test)
{
    test_with_reference("c_extension/caddi4spn-01.elf",
                        "c_extension/references/caddi4spn-01.reference_output");
}

TEST(c_extension, cand_test)
{
    test_with_reference("c_extension/cand-01.elf",
                        "c_extension/references/cand-01.reference_output");
}

TEST(c_extension, candi_test)
{
    test_with_reference("c_extension/candi-01.elf",
                        "c_extension/references/candi-01.reference_output");
}

TEST(c_extension, cbeqz_test)
{
    test_with_reference("c_extension/cbeqz-01.elf",
                        "c_extension/references/cbeqz-01.reference_output");
}

TEST(c_extension, cbnez_test)
{
    test_with_reference("c_extension/cbnez-01.elf",
                        "c_extension/references/cbnez-01.reference_output");
}

TEST(c_extension, cebreak_test)
{
    test_with_reference("c_extension/cebreak-01.elf",
                        "c_extension/references/cebreak-01.reference_output");
}

TEST(c_extension, cj_test)
{
    test_with_reference("c_extension/cj-01.elf", "c_extension/references/cj-01.reference_output");
}

TEST(c_extension, cjal_test)
{
    test_with_reference("c_extension/cjal-01.elf",
                        "c_extension/references/cjal-01.reference_output");
}

TEST(c_extension, cjalr_test)
{
    test_with_reference("c_extension/cjalr-01.elf",
                        "c_extension/references/cjalr-01.reference_output");
}

TEST(c_extension, cjr_test)
{
    test_with_reference("c_extension/cjr-01.elf", "c_extension/references/cjr-01.reference_output");
}

TEST(c_extension, cli_test)
{
    test_with_reference("c_extension/cli-01.elf", "c_extension/references/cli-01.reference_output");
}

TEST(c_extension, clui_test)
{
    test_with_reference("c_extension/clui-01.elf",
                        "c_extension/references/clui-01.reference_output");
}

TEST(c_extension, clw_test)
{
    test_with_reference("c_extension/clw-01.elf", "c_extension/references/clw-01.reference_output");
}

TEST(c_extension, clwsp_test)
{
    test_with_reference("c_extension/clwsp-01.elf",
                        "c_extension/references/clwsp-01.reference_output");
}

TEST(c_extension, cmv_test)
{
    test_with_reference("c_extension/cmv-01.elf", "c_extension/references/cmv-01.reference_output");
}

TEST(c_extension, cnop_test)
{
    test_with_reference("c_extension/cnop-01.elf",
                        "c_extension/references/cnop-01.reference_output");
}

TEST(c_extension, cor_test)
{
    test_with_reference("c_extension/cor-01.elf", "c_extension/references/cor-01.reference_output");
}

TEST(c_extension, cslli_test)
{
    test_with_reference("c_extension/cslli-01.elf",
                        "c_extension/references/cslli-01.reference_output");
}

TEST(c_extension, csrai_test)
{
    test_with_reference("c_extension/csrai-01.elf",
                        "c_extension/references/csrai-01.reference_output");
}

TEST(c_extension, csrli_test)
{
    test_with_reference("c_extension/csrli-01.elf",
                        "c_extension/references/csrli-01.reference_output");
}

TEST(c_extension, csub_test)
{
    test_with_reference("c_extension/csub-01.elf",
                        "c_extension/references/csub-01.reference_output");
}

TEST(c_extension, csw_test)
{
    test_with_reference("c_extension/csw-01.elf", "c_extension/references/csw-01.reference_output");
}

TEST(c_extension, cswsp_test)
{
    test_with_reference("c_extension/cswsp-01.elf",
                        "c_extension/references/cswsp-01.reference_output");
}

TEST(c_extension, cxor_test)
{
    test_with_reference("c_extension/cxor-01.elf",
                        "c_extension/references/cxor-01.reference_output");
}

#include "sc_main.cpp"
