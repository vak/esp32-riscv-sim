#include <gtest/gtest.h>
#include "simulator.h"

//
// Compare the signature contents to the reference.
//
void Simulator::check_signature(const std::string &filename)
{
    // Get address of the signature area.
    ASSERT_TRUE(begin_signature > 0);
    ASSERT_TRUE(end_signature > 0);

    // Open the the reference file.
    std::fstream ref_file;
    ref_file.open(filename);
    ASSERT_TRUE(ref_file.is_open());

    // Scan the reference file and compare.
    for (unsigned index = 0; ; index++) {
        auto addr = begin_signature + index*4;
        if (addr >= end_signature) {
            ASSERT_TRUE(index > 0);
            break;
        }

        // Get expected value.
        std::string line;
        ASSERT_TRUE(getline(ref_file, line));
        uint32_t expect = std::stoul(line, nullptr, 16);

        // Compare to the result.
        uint32_t result = debug_load32(addr);
        ASSERT_EQ(result, expect)
            << "Signature index " << index << std::hex
            << ": result 0x" << result
            << ", expect 0x" << expect;
    }
}

//
// Run ELF file and compare to the reference.
//
void test_with_reference(std::string elf_name, std::string ref_name)
{
    // Instantiate the simulator.
    Simulator sim("top");

    // Load the binary image.
    sim.read_elf_file(TEST_DIR "/" + elf_name);

    // Run simulation.
    sc_core::sc_start();

    // Check results.
    EXPECT_TRUE(sim.get_instructions_executed() > 0);
    sim.check_signature(TEST_DIR "/" + ref_name);
}
