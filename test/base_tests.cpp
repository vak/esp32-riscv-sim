#include <gtest/gtest.h>
#include <systemc.h>

#include "simulator.h"

TEST(rv32i, add_test)
{
    test_with_reference("rv32i/add-01.elf", "rv32i/references/add-01.reference_output");
}

TEST(rv32i, addi_test)
{
    test_with_reference("rv32i/addi-01.elf", "rv32i/references/addi-01.reference_output");
}

TEST(rv32i, and_test)
{
    test_with_reference("rv32i/and-01.elf", "rv32i/references/and-01.reference_output");
}

TEST(rv32i, andi_test)
{
    test_with_reference("rv32i/andi-01.elf", "rv32i/references/andi-01.reference_output");
}

TEST(rv32i, auipc_test)
{
    test_with_reference("rv32i/auipc-01.elf", "rv32i/references/auipc-01.reference_output");
}

TEST(rv32i, beq_test)
{
    test_with_reference("rv32i/beq-01.elf", "rv32i/references/beq-01.reference_output");
}

TEST(rv32i, bge_test)
{
    test_with_reference("rv32i/bge-01.elf", "rv32i/references/bge-01.reference_output");
}

TEST(rv32i, bgeu_test)
{
    test_with_reference("rv32i/bgeu-01.elf", "rv32i/references/bgeu-01.reference_output");
}

TEST(rv32i, blt_test)
{
    test_with_reference("rv32i/blt-01.elf", "rv32i/references/blt-01.reference_output");
}

TEST(rv32i, bltu_test)
{
    test_with_reference("rv32i/bltu-01.elf", "rv32i/references/bltu-01.reference_output");
}

TEST(rv32i, bne_test)
{
    test_with_reference("rv32i/bne-01.elf", "rv32i/references/bne-01.reference_output");
}

TEST(rv32i, fence_test)
{
    test_with_reference("rv32i/fence-01.elf", "rv32i/references/fence-01.reference_output");
}

TEST(rv32i, jal_test)
{
    test_with_reference("rv32i/jal-01.elf", "rv32i/references/jal-01.reference_output");
}

TEST(rv32i, jalr_test)
{
    test_with_reference("rv32i/jalr-01.elf", "rv32i/references/jalr-01.reference_output");
}

TEST(rv32i, lb_align_test)
{
    test_with_reference("rv32i/lb-align-01.elf", "rv32i/references/lb-align-01.reference_output");
}

TEST(rv32i, lbu_align_test)
{
    test_with_reference("rv32i/lbu-align-01.elf", "rv32i/references/lbu-align-01.reference_output");
}

TEST(rv32i, lh_align_test)
{
    test_with_reference("rv32i/lh-align-01.elf", "rv32i/references/lh-align-01.reference_output");
}

TEST(rv32i, lhu_align_test)
{
    test_with_reference("rv32i/lhu-align-01.elf", "rv32i/references/lhu-align-01.reference_output");
}

TEST(rv32i, lui_test)
{
    test_with_reference("rv32i/lui-01.elf", "rv32i/references/lui-01.reference_output");
}

TEST(rv32i, lw_align_test)
{
    test_with_reference("rv32i/lw-align-01.elf", "rv32i/references/lw-align-01.reference_output");
}

TEST(rv32i, or_test)
{
    test_with_reference("rv32i/or-01.elf", "rv32i/references/or-01.reference_output");
}

TEST(rv32i, ori_test)
{
    test_with_reference("rv32i/ori-01.elf", "rv32i/references/ori-01.reference_output");
}

TEST(rv32i, sb_align_test)
{
    test_with_reference("rv32i/sb-align-01.elf", "rv32i/references/sb-align-01.reference_output");
}

TEST(rv32i, sh_align_test)
{
    test_with_reference("rv32i/sh-align-01.elf", "rv32i/references/sh-align-01.reference_output");
}

TEST(rv32i, sll_test)
{
    test_with_reference("rv32i/sll-01.elf", "rv32i/references/sll-01.reference_output");
}

TEST(rv32i, slli_test)
{
    test_with_reference("rv32i/slli-01.elf", "rv32i/references/slli-01.reference_output");
}

TEST(rv32i, slt_test)
{
    test_with_reference("rv32i/slt-01.elf", "rv32i/references/slt-01.reference_output");
}

TEST(rv32i, slti_test)
{
    test_with_reference("rv32i/slti-01.elf", "rv32i/references/slti-01.reference_output");
}

TEST(rv32i, sltiu_test)
{
    test_with_reference("rv32i/sltiu-01.elf", "rv32i/references/sltiu-01.reference_output");
}

TEST(rv32i, sltu_test)
{
    test_with_reference("rv32i/sltu-01.elf", "rv32i/references/sltu-01.reference_output");
}

TEST(rv32i, sra_test)
{
    test_with_reference("rv32i/sra-01.elf", "rv32i/references/sra-01.reference_output");
}

TEST(rv32i, srai_test)
{
    test_with_reference("rv32i/srai-01.elf", "rv32i/references/srai-01.reference_output");
}

TEST(rv32i, srl_test)
{
    test_with_reference("rv32i/srl-01.elf", "rv32i/references/srl-01.reference_output");
}

TEST(rv32i, srli_test)
{
    test_with_reference("rv32i/srli-01.elf", "rv32i/references/srli-01.reference_output");
}

TEST(rv32i, sub_test)
{
    test_with_reference("rv32i/sub-01.elf", "rv32i/references/sub-01.reference_output");
}

TEST(rv32i, sw_align_test)
{
    test_with_reference("rv32i/sw-align-01.elf", "rv32i/references/sw-align-01.reference_output");
}

TEST(rv32i, xor_test)
{
    test_with_reference("rv32i/xor-01.elf", "rv32i/references/xor-01.reference_output");
}

TEST(rv32i, xori_test)
{
    test_with_reference("rv32i/xori-01.elf", "rv32i/references/xori-01.reference_output");
}

#include "sc_main.cpp"
