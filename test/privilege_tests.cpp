#include <gtest/gtest.h>
#include <systemc.h>

#include "simulator.h"

TEST(privilege, ebreak_test)
{
    test_with_reference("privilege/ebreak.elf", "privilege/references/ebreak.reference_output");
}

TEST(privilege, ecall_test)
{
    test_with_reference("privilege/ecall.elf", "privilege/references/ecall.reference_output");
}

TEST(privilege, misalign1_jalr_test)
{
    test_with_reference("privilege/misalign1-jalr-01.elf",
                        "privilege/references/misalign1-jalr-01.reference_output");
}

TEST(privilege, misalign2_jalr_test)
{
    test_with_reference("privilege/misalign2-jalr-01.elf",
                        "privilege/references/misalign2-jalr-01.reference_output");
}

TEST(privilege, misalign_beq_test)
{
    test_with_reference("privilege/misalign-beq-01.elf",
                        "privilege/references/misalign-beq-01.reference_output");
}

TEST(privilege, misalign_bge_test)
{
    test_with_reference("privilege/misalign-bge-01.elf",
                        "privilege/references/misalign-bge-01.reference_output");
}

TEST(privilege, misalign_bgeu_test)
{
    test_with_reference("privilege/misalign-bgeu-01.elf",
                        "privilege/references/misalign-bgeu-01.reference_output");
}

TEST(privilege, misalign_blt_test)
{
    test_with_reference("privilege/misalign-blt-01.elf",
                        "privilege/references/misalign-blt-01.reference_output");
}

TEST(privilege, misalign_bltu_test)
{
    test_with_reference("privilege/misalign-bltu-01.elf",
                        "privilege/references/misalign-bltu-01.reference_output");
}

TEST(privilege, misalign_bne_test)
{
    test_with_reference("privilege/misalign-bne-01.elf",
                        "privilege/references/misalign-bne-01.reference_output");
}

TEST(privilege, misalign_jal_test)
{
    test_with_reference("privilege/misalign-jal-01.elf",
                        "privilege/references/misalign-jal-01.reference_output");
}

TEST(privilege, misalign_lh_test)
{
    test_with_reference("privilege/misalign-lh-01.elf",
                        "privilege/references/misalign-lh-01.reference_output");
}

TEST(privilege, misalign_lhu_test)
{
    test_with_reference("privilege/misalign-lhu-01.elf",
                        "privilege/references/misalign-lhu-01.reference_output");
}

TEST(privilege, misalign_lw_test)
{
    test_with_reference("privilege/misalign-lw-01.elf",
                        "privilege/references/misalign-lw-01.reference_output");
}

TEST(privilege, misalign_sh_test)
{
    test_with_reference("privilege/misalign-sh-01.elf",
                        "privilege/references/misalign-sh-01.reference_output");
}

TEST(privilege, misalign_sw_test)
{
    test_with_reference("privilege/misalign-sw-01.elf",
                        "privilege/references/misalign-sw-01.reference_output");
}

#include "sc_main.cpp"
