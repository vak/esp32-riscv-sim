#include "debug_io.h"

int debug_printf(const char *fmt, ...)
{
    va_list args;
    int err;

    va_start(args, fmt);
    err = custom_vprintf(debug_putc, fmt, args);
    va_end(args);
    return err;
}

int debug_vprintf(const char *fmt, va_list args)
{
    int err;

    err = custom_vprintf(debug_putc, fmt, args);
    return err;
}
