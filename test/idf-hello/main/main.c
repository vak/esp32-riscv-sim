#include "debug_io.h"
#include "esp32c3/rom/ets_sys.h"
#include "soc/soc.h"
#include "soc/timer_group_reg.h"
#include "soc/interrupt_core0_reg.h"
#include "riscv/csr.h"

void app_main()
{
    // Master interrupt disable.
    unsigned save_mstatus = RV_CLEAR_CSR(mstatus, MSTATUS_MIE);
    debug_printf("mstatus = %08x\n", save_mstatus);

    // Print mask of enabled interrupts: 0700009c
    debug_printf("enabled interrupts = %08x\n", REG_READ(INTERRUPT_CORE0_CPU_INT_ENABLE_REG));
    //REG_WRITE(INTERRUPT_CORE0_CPU_INT_ENABLE_REG, 0);
    //RV_WRITE_CSR(mstatus, save_mstatus);

    for (unsigned cnt = 0; ; cnt++) {
        debug_printf("Hello, RISC-V! %u\n", cnt);

        // Delay for 1 second.
        ets_delay_us(1000000);

        // Feed watchdogs of timer groups 0 and 1.
        REG_WRITE(TIMG_WDTWPROTECT_REG(0), TIMG_WDT_WKEY_VALUE);
        REG_WRITE(TIMG_WDTFEED_REG(0), 0);
        REG_WRITE(TIMG_WDTWPROTECT_REG(1), TIMG_WDT_WKEY_VALUE);
        REG_WRITE(TIMG_WDTFEED_REG(1), 0);
    }
}
