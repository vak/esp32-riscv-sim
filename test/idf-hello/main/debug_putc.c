#include "debug_io.h"
#include "soc/soc.h"
#include "soc/uart_reg.h"

//
// Send a byte to the UART transmitter, with interrupts disabled.
//
void debug_putc(int c)
{
    // Disable UART interrupt.
    //TODO: restore global interrupts.
    unsigned x = READ_PERI_REG(UART_INT_ENA_REG(0));
    WRITE_PERI_REG(UART_INT_ENA_REG(0), 0);

    // Wait for transmitter FIFO empty.
    while ((READ_PERI_REG(UART_STATUS_REG(0)) & UART_TXFIFO_CNT_M) != 0)
        continue;

again:
    // Send byte.
    WRITE_PERI_REG(UART_FIFO_REG(0), (uint8_t) c);

    // Wait for transmitter FIFO empty.
    while ((READ_PERI_REG(UART_STATUS_REG(0)) & UART_TXFIFO_CNT_M) != 0)
        continue;

    //watchdog_alive();
    if (c == '\n') {
        c = '\r';
        goto again;
    }
    // Restore UART interrupt.
    WRITE_PERI_REG(UART_INT_ENA_REG(0), x);
}
