#include "debug_io.h"

//
// Wait for the byte to be received and return it.
//
int debug_getc(void)
{
    unsigned char c;

    for (;;) {
        int x = 0;
        mips_intr_disable(&x);
        //watchdog_alive();

        // Wait until receive data available.
        if (UxSTA & PIC32_USTA_URXDA) {
            c = UxRXREG;
            break;
        }
        mips_intr_restore(x);
    }
    return c;
}
