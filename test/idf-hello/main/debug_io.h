#include <stdarg.h>

void debug_putc(int c);
int debug_getc(void);
int debug_printf(const char *fmt, ...);
int debug_vprintf(const char *fmt, va_list args);
int custom_vprintf(void (*putc)(char), char const *fmt, va_list ap);
