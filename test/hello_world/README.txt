
Project build complete. To flash, run this command:

    /Users/vak/.espressif/python_env/idf4.4_py3.9_env/bin/python
        ../../../Project/RISC-V/esp-idf/components/esptool_py/esptool/esptool.py
        -p (PORT) -b 460800 --before default_reset --after hard_reset
        --chip esp32c3 write_flash --flash_mode dio --flash_size detect --flash_freq 80m
        0x0 build/bootloader/bootloader.bin
        0x8000 build/partition_table/partition-table.bin
        0x10000 build/hello-world.bin

or run 'idf.py -p (PORT) flash'

Flash memory map:

    Address Binary
    --------------
        0x0 build/bootloader/bootloader.bin
     0x8000 build/partition_table/partition-table.bin
    0x10000 build/hello-world.bin

Bootloader segments:
    Addr        Size    Access  Sections
    ------------------------------------
    3fcd6000    0x1754  RW-     .dram0.bss .dram0.data .dram0.rodata
    403ce000    0x8b4   R-E     .iram.text
    403d0000    0x2ba2  R-E     .iram_loader.text

Application segments:
    Addr        Size    Access  Sections
    ------------------------------------
    3c000020    0x26508 RW-     .flash_rodata_dummy .flash.appdesc .flash.rodata
    3fc80000    0xba40  RW-     .dram0.dummy .dram0.data .dram0.bss .dram0.heap_start
    40380000    0x9800  RWE     .iram0.text .iram0.text_end
    42000020    0x15a4c R-E     .flash.text
    50000000    0x10    RW-     .rtc.text .rtc.dummy .rtc.force_fast .rtc.data .rtc_noinit .rtc.force_slow .noinit .iram0.data .iram0.bss

CPU Address Map:
    Name    Description         Start Address   End Address Size
    --------------------------------------------------------------
    IRAM    Instruction Memory  4000_0000       47FF_FFFF   128 MB
    DRAM    Data Memory         3800_0000       3FFF_FFFF   128 MB
    DM      Debug Memory        2000_0000       27FF_FFFF   128 MB
    AHB     AHB Space           default         default

Address Mapping:
    Bus Type                Low Address High Address    Size    Target
    -------------------------------------------------------------------
    Data bus                3C00_0000   3C7F_FFFF       8 MB    External memory
    Data bus                3FC8_0000   3FCD_FFFF       384 KB  Internal memory
    Data bus                3FF0_0000   3FF1_FFFF       128 KB  Internal memory
    Instruction bus         4000_0000   4005_FFFF       384 KB  Internal memory
    Instruction bus         4037_C000   403D_FFFF       400 KB  Internal memory
    Instruction bus         4200_0000   427F_FFFF       8 MB    External memory
    Data/Instruction bus    5000_0000   5000_1FFF       8 KB    Internal memory
    Data/Instruction bus    6000_0000   600D_0FFF       836 KB  Peripherals

Internal Memory Address Mapping:
    Bus Type                Low Address High Address    Size    Target
    ----------------------------------------------------------------------
    Data bus                3FF0_0000   3FF1_FFFF       128 KB  Internal ROM 1 (primary bootloader data)
                            3FC8_0000   3FCD_FFFF       384 KB  Internal SRAM 1
    ----------------------------------------------------------------------
    Instruction bus         4000_0000   4003_FFFF       256 KB  Internal ROM 0 (primary bootloader code)
                            4004_0000   4005_FFFF       128 KB  Internal ROM 1 (primary bootloader data)
                            4037_C000   4037_FFFF       16 KB   Internal SRAM 0 (I cache)
                            4038_0000   403D_FFFF       384 KB  Internal SRAM 1
    ----------------------------------------------------------------------
    Data/Instruction bus    5000_0000   5000_1FFF       8 KB    RTC FAST Memory

External Memory Address Mapping:
    Bus Type                Low Address High Address    Size    Target
    ----------------------------------------------------------------------
    Data bus                3C00_0000   3C7F_FFFF       8 MB    Uniform Cache (read-only)
    Instruction bus         4200_0000   427F_FFFF       8 MB    Uniform Cache
