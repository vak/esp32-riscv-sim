#include <gtest/gtest.h>
#include <systemc.h>

#include "simulator.h"

TEST(m_extension, div_test)
{
    test_with_reference("m_extension/div-01.elf", "m_extension/references/div-01.reference_output");
}

TEST(m_extension, divu_test)
{
    test_with_reference("m_extension/divu-01.elf",
                        "m_extension/references/divu-01.reference_output");
}

TEST(m_extension, mul_test)
{
    test_with_reference("m_extension/mul-01.elf", "m_extension/references/mul-01.reference_output");
}

TEST(m_extension, mulh_test)
{
    test_with_reference("m_extension/mulh-01.elf",
                        "m_extension/references/mulh-01.reference_output");
}

TEST(m_extension, mulhsu_test)
{
    test_with_reference("m_extension/mulhsu-01.elf",
                        "m_extension/references/mulhsu-01.reference_output");
}

TEST(m_extension, mulhu_test)
{
    test_with_reference("m_extension/mulhu-01.elf",
                        "m_extension/references/mulhu-01.reference_output");
}

TEST(m_extension, rem_test)
{
    test_with_reference("m_extension/rem-01.elf", "m_extension/references/rem-01.reference_output");
}

TEST(m_extension, remu_test)
{
    test_with_reference("m_extension/remu-01.elf",
                        "m_extension/references/remu-01.reference_output");
}

#include "sc_main.cpp"
