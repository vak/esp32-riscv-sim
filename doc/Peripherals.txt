Table 3-4. Module/Peripheral Address Mapping

Target                          Address     High        C++ Define
------------------------------------------------------------------------
UART Controller 0               6000_0000   6000_0FFF   SOC_PERIPHERAL_LOW, DR_REG_UART_BASE
                    (Reserved)  6000_1000   6000_1FFF
SPI Controller 1                6000_2000   6000_2FFF   DR_REG_SPI1_BASE
SPI Controller 0                6000_3000   6000_3FFF   DR_REG_SPI0_BASE
GPIO                            6000_4000   6000_4FFF   DR_REG_GPIO_BASE
                    (Reserved)  6000_5000   6000_6FFF   DR_REG_FE2_BASE, DR_REG_FE_BASE
TIMER                           6000_7000   6000_7FFF
Low-Power Management            6000_8000   6000_8FFF   DR_REG_RTCCNTL_BASE, DR_REG_EFUSE_BASE
IO MUX                          6000_9000   6000_9FFF   DR_REG_IO_MUX_BASE
                    (Reserved)  6000_A000   6000_FFFF   DR_REG_RTC_I2C_BASE
UART Controller 1               6001_0000   6001_0FFF   DR_REG_UART1_BASE
                    (Reserved)  6001_1000   6001_2FFF
I2C Controller                  6001_3000   6001_3FFF   DR_REG_I2C_EXT_BASE
UHCI0                           6001_4000   6001_4FFF   DR_REG_UHCI0_BASE
                    (Reserved)  6001_5000   6001_5FFF
Remote Control Peripheral       6001_6000   6001_6FFF   DR_REG_RMT_BASE
                    (Reserved)  6001_7000   6001_8FFF
LED Control PWM                 6001_9000   6001_9FFF   DR_REG_LEDC_BASE
eFuse Controller                6001_A000   6001_AFFF
                    (Reserved)  6001_B000   6001_EFFF   DR_REG_NRX_BASE, DR_REG_BB_BASE
Timer Group 0                   6001_F000   6001_FFFF   DR_REG_TIMERGROUP0_BASE
Timer Group 1                   6002_0000   6002_0FFF   DR_REG_TIMERGROUP1_BASE
                    (Reserved)  6002_1000   6002_2FFF
System Timer                    6002_3000   6002_3FFF   DR_REG_SYSTIMER_BASE
SPI Controller 2                6002_4000   6002_4FFF   DR_REG_SPI2_BASE
                    (Reserved)  6002_5000   6002_5FFF
APB Controller                  6002_6000   6002_6FFF   DR_REG_SYSCON_BASE
                    (Reserved)  6002_7000   6002_AFFF
Two-wire Automotive Interface   6002_B000   6002_BFFF   DR_REG_TWAI_BASE
                    (Reserved)  6002_C000   6002_CFFF
I2S Controller                  6002_D000   6002_DFFF   DR_REG_I2S0_BASE
                    (Reserved)  6002_E000   6003_9FFF
AES Accelerator                 6003_A000   6003_AFFF   DR_REG_AES_BASE
SHA Accelerator                 6003_B000   6003_BFFF   DR_REG_SHA_BASE
RSA Accelerator                 6003_C000   6003_CFFF   DR_REG_RSA_BASE
Digital Signature               6003_D000   6003_DFFF   DR_REG_DIGITAL_SIGNATURE_BASE
HMAC Accelerator                6003_E000   6003_EFFF   DR_REG_HMAC_BASE
GDMA Controller                 6003_F000   6003_FFFF   DR_REG_GDMA_BASE
ADC Controller                  6004_0000   6004_0FFF   DR_REG_APB_SARADC_BASE
                    (Reserved)  6004_1000   6002_FFFF
USB Serial/JTAG Controller      6004_3000   6004_3FFF   SOC_DPORT_USB_BASE
                    (Reserved)  6004_4000   600B_FFFF
System Registers                600C_0000   600C_0FFF   DR_REG_SYSTEM_BASE
Sensitive Register              600C_1000   600C_1FFF   DR_REG_SENSITIVE_BASE
Interrupt Matrix                600C_2000   600C_2FFF   DR_REG_INTERRUPT_BASE
                    (Reserved)  600C_3000   600C_3FFF
Configure Cache                 600C_4000   600C_BFFF   DR_REG_EXTMEM_BASE, DR_REG_MMU_TABLE
Ext.Memory En/Decryption        600C_C000   600C_CFFF   DR_REG_AES_XTS_BASE
                    (Reserved)  600C_D000   600C_DFFF
Assist Debug                    600C_E000   600C_EFFF   DR_REG_ASSIST_DEBUG_BASE
                    (Reserved)  600C_F000   600C_FFFF   DR_REG_DEDICATED_GPIO_BASE
World Controller                600D_0000   600D_0FFF   DR_REG_WORLD_CNTL_BASE, DR_REG_DPORT_END
------------------------------------------------------------------------
