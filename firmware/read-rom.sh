#!/bin/sh
#
# Read internal ROM of ESP32-C3 microcontroller.
# It contains the primary bootloader.
#

#
# Code: Internal ROM 0 at address 4000_0000 - 4003_FFFF, 256 kbytes
#
esptool.py dump_mem 0x40000000 262144 irom0.bin

#
# Data: Internal ROM 1 at address 3FF0_0000 - 3FF1_FFFF, 128 kbytes
#
esptool.py dump_mem 0x3ff00000 131072 drom1.bin
