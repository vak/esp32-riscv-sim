/*!
 \file A_extension.h
 \brief Implement A extensions part of the RISC-V
 \author Màrius Montón
 \date December 2018
 */
// SPDX-License-Identifier: GPL-3.0-or-later

#include "a_extension.h"
#include "processor.h"

op_A_Codes A_Extension::decode() const
{
    switch (opcode()) {
    case A_LR:
        return OP_A_LR;
        break;
    case A_SC:
        return OP_A_SC;
        break;
    case A_AMOSWAP:
        return OP_A_AMOSWAP;
        break;
    case A_AMOADD:
        return OP_A_AMOADD;
        break;
    case A_AMOXOR:
        return OP_A_AMOXOR;
        break;
    case A_AMOAND:
        return OP_A_AMOAND;
        break;
    case A_AMOOR:
        return OP_A_AMOOR;
        break;
    case A_AMOMIN:
        return OP_A_AMOMIN;
        break;
    case A_AMOMAX:
        return OP_A_AMOMAX;
        break;
    case A_AMOMINU:
        return OP_A_AMOMINU;
        break;
    case A_AMOMAXU:
        return OP_A_AMOMAXU;
        break;
    default:
        return OP_A_ERROR;
        break;
    }

    return OP_A_ERROR;
}

bool A_Extension::Exec_A_LR()
{
    uint32_t mem_addr = 0;
    int rd, rs1, rs2;
    uint32_t data;

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    if (rs2 != 0) {
        std::cout << "ILEGAL INSTRUCTION, LR.W: rs2 != 0" << std::endl;
        cpu.raise_exception(EXCEPTION_CAUSE_ILLEGAL_INSTRUCTION, m_instr);

        return false;
    }

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);
    cpu.set_reg(rd, static_cast<int32_t>(data));

    TLB_reserve(mem_addr);

    // Log::out() << std::dec << "LR.W: x" << rs1 << " (@0x" << std::hex << mem_addr
    //                        << std::dec << ") -> x" << rd << std::endl;

    return true;
}

bool A_Extension::Exec_A_SC()
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.get_reg(rs2);

    if (TLB_reserved(mem_addr)) {
        cpu.data_write32(mem_addr, data);
        cpu.set_reg(rd, 0); // SC writes 0 to rd on success
    } else {
        cpu.set_reg(rd, 1); // SC writes nonzero on failure
    }

    // Log::out() << std::dec << "SC.W: (@0x" << std::hex << mem_addr << std::dec
    //                        << ") <- x" << rs2 << std::hex << "(0x" << data << ")" << std::dec << std::endl;

    return true;
}

bool A_Extension::Exec_A_AMOSWAP() const
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;
    uint32_t aux;

    /* These instructions must be atomic */

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);
    cpu.set_reg(rd, static_cast<int32_t>(data));

    // swap
    aux = cpu.get_reg(rs2);
    cpu.set_reg(rs2, static_cast<int32_t>(data));

    cpu.data_write32(mem_addr, aux);

    // Log::out() << std::dec << "AMOSWAP " << std::endl;
    return true;
}

bool A_Extension::Exec_A_AMOADD() const
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;

    /* These instructions must be atomic */

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);

    cpu.set_reg(rd, static_cast<int32_t>(data));

    // add
    data = data + cpu.get_reg(rs2);

    cpu.data_write32(mem_addr, data);

    // Log::out() << std::dec << "AMOADD " << std::endl;

    return true;
}

bool A_Extension::Exec_A_AMOXOR() const
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;

    /* These instructions must be atomic */

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);

    cpu.set_reg(rd, static_cast<int32_t>(data));

    // add
    data = data ^ cpu.get_reg(rs2);

    cpu.data_write32(mem_addr, data);

    // Log::out() << std::dec << "AMOXOR " << std::endl;

    return true;
}

bool A_Extension::Exec_A_AMOAND() const
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;

    /* These instructions must be atomic */

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);

    cpu.set_reg(rd, static_cast<int32_t>(data));

    // add
    data = data & cpu.get_reg(rs2);

    cpu.data_write32(mem_addr, data);

    // Log::out() << std::dec << "AMOAND " << std::endl;

    return true;
}

bool A_Extension::Exec_A_AMOOR() const
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;

    /* These instructions must be atomic */

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);

    cpu.set_reg(rd, static_cast<int32_t>(data));

    // add
    data = data | cpu.get_reg(rs2);

    cpu.data_write32(mem_addr, data);

    // Log::out() << std::dec << "AMOOR " << std::endl;
    return true;
}

bool A_Extension::Exec_A_AMOMIN() const
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;
    uint32_t aux;

    /* These instructions must be atomic */

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);

    cpu.set_reg(rd, static_cast<int32_t>(data));

    // min
    aux = cpu.get_reg(rs2);
    if ((int32_t)data < (int32_t)aux) {
        aux = data;
    }

    cpu.data_write32(mem_addr, aux);

    // Log::out() << std::dec << "AMOMIN " << std::endl;

    return true;
}

bool A_Extension::Exec_A_AMOMAX() const
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;
    uint32_t aux;

    /* These instructions must be atomic */

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);

    cpu.set_reg(rd, static_cast<int32_t>(data));

    // >
    aux = cpu.get_reg(rs2);
    if ((int32_t)data > (int32_t)aux) {
        aux = data;
    }

    cpu.data_write32(mem_addr, aux);

    // Log::out() << std::dec << "AMOMAX " << std::endl;

    return true;
}

bool A_Extension::Exec_A_AMOMINU() const
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;
    uint32_t aux;

    /* These instructions must be atomic */

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);

    cpu.set_reg(rd, static_cast<int32_t>(data));

    // min
    aux = cpu.get_reg(rs2);
    if (data < aux) {
        aux = data;
    }

    cpu.data_write32(mem_addr, aux);

    // Log::out() << std::dec << "AMOMINU " << std::endl;

    return true;
}

bool A_Extension::Exec_A_AMOMAXU() const
{
    uint32_t mem_addr;
    int rd, rs1, rs2;
    uint32_t data;
    uint32_t aux;

    /* These instructions must be atomic */

    rd = get_rd();
    rs1 = get_rs1();
    rs2 = get_rs2();

    mem_addr = cpu.get_reg(rs1);
    data = cpu.data_read32(mem_addr);

    cpu.set_reg(rd, static_cast<int32_t>(data));

    // max
    aux = cpu.get_reg(rs2);
    if (data > aux) {
        aux = data;
    }

    cpu.data_write32(mem_addr, aux);

    // Log::out() << std::dec << "AMOMAXU " << std::endl;

    return true;
}

void A_Extension::TLB_reserve(uint32_t address)
{
    TLB_A_Entries.insert(address);
}

bool A_Extension::TLB_reserved(uint32_t address)
{
    if (TLB_A_Entries.count(address) == 1) {
        TLB_A_Entries.erase(address);
        return true;
    } else {
        return false;
    }
}

bool A_Extension::process_instruction(uint32_t instruction)
{
    bool PC_not_affected = true;

    setInstr(instruction);

    switch (decode()) {
    case OP_A_LR:
        Exec_A_LR();
        break;
    case OP_A_SC:
        Exec_A_SC();
        break;
    case OP_A_AMOSWAP:
        Exec_A_AMOSWAP();
        break;
    case OP_A_AMOADD:
        Exec_A_AMOADD();
        break;
    case OP_A_AMOXOR:
        Exec_A_AMOXOR();
        break;
    case OP_A_AMOAND:
        Exec_A_AMOAND();
        break;
    case OP_A_AMOOR:
        Exec_A_AMOOR();
        break;
    case OP_A_AMOMIN:
        Exec_A_AMOMIN();
        break;
    case OP_A_AMOMAX:
        Exec_A_AMOMAX();
        break;
    case OP_A_AMOMINU:
        Exec_A_AMOMINU();
        break;
    case OP_A_AMOMAXU:
        Exec_A_AMOMAXU();
        break;
    default:
        std::cout << "A instruction not implemented yet" << std::endl;
        break;
    }

    return PC_not_affected;
}
