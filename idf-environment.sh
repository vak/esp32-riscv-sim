
export IDF_PATH="$HOME/Project/RISC-V/esp-idf"
export IDF_TOOLS_EXPORT_CMD="$IDF_PATH/export.sh"
export IDF_TOOLS_INSTALL_CMD="$IDF_PATH/install.sh"
export IDF_PYTHON_ENV_PATH="$HOME/.espressif/python_env/idf4.4_py3.9_env"

export PATH="$IDF_PATH/components/esptool_py/esptool:$IDF_PATH/components/espcoredump:$IDF_PATH/components/partition_table:$IDF_PATH/components/app_update:$HOME/.espressif/tools/xtensa-esp32-elf/esp-2020r3-8.4.0/xtensa-esp32-elf/bin:$HOME/.espressif/tools/xtensa-esp32s2-elf/esp-2020r3-8.4.0/xtensa-esp32s2-elf/bin:$HOME/.espressif/tools/xtensa-esp32s3-elf/esp-2020r3-8.4.0/xtensa-esp32s3-elf/bin:$HOME/.espressif/tools/riscv32-esp-elf/1.24.0.123_64eb9ff-8.4.0/riscv32-esp-elf/bin:$HOME/.espressif/tools/esp32ulp-elf/2.28.51-esp-20191205/esp32ulp-elf-binutils/bin:$HOME/.espressif/tools/esp32s2ulp-elf/2.28.51-esp-20191205/esp32s2ulp-elf-binutils/bin:$HOME/.espressif/tools/openocd-esp32/v0.10.0-esp32-20200709/openocd-esp32/bin:$HOME/.espressif/python_env/idf4.4_py3.9_env/bin:$IDF_PATH/tools":$PATH

export OPENOCD_SCRIPTS="$HOME/.espressif/tools/openocd-esp32/v0.10.0-esp32-20200709/openocd-esp32/share/openocd/scripts"
